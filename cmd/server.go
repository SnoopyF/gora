package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	"github.com/nfnt/resize"
	"image"
	"image/jpeg"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type App struct {
	DB  *sql.DB
	Env Env
}

type Env struct {
	StoragePath string
}

func initApp() (*App, error) {
	db, err := sql.Open("sqlite3", "/server.sqlite")
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return &App{
		DB: db,
		Env: Env{
			StoragePath: getEnv("GORA_FILE_STORAGE_PATH", "/storage"),
		},
	}, nil
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}


func main() {
	app, err := initApp()
	if err != nil {
		log.Println(err)
		return
	}

	http.HandleFunc("/upload", app.upload)
	http.HandleFunc("/list", app.list)
	http.HandleFunc("/preview", app.preview)
	http.HandleFunc("/delete", app.delete)

	fmt.Println("Server is running ...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
func (a *App) delete(w http.ResponseWriter, r *http.Request) {
	key := r.URL.Query().Get("id")
	if key == "" {
		log.Println(errors.New("url param 'id' is missing"))
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	fileId, err := strconv.Atoi(key)
	if err != nil {
		ServerError(err, w)
		return
	}

	file, err := a.getFile(fileId)
	if err != nil {
		ServerError(err, w)
		return
	}

	if err := os.Remove(file.Path); err != nil {
		log.Println(err)
		ServerError(err, w)
		return
	}

	if err := a.delFile(fileId); err != nil {
		ServerError(err, w)
		return
	}

	if err := json.NewEncoder(w).Encode(ResponseStatus{Status: true}); err != nil {
		ServerError(err, w)
		return
	}
}

func (a *App) upload(w http.ResponseWriter, r *http.Request) {

	_ = r.ParseMultipartForm(10 << 20)

	file, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	defer func() {
		_ = file.Close()
	}()

	if handler.Header.Get("Content-Type") != "image/jpeg" {
		http.Error(w, http.StatusText(http.StatusUnsupportedMediaType), http.StatusUnsupportedMediaType)
	}

	genFileName := fmt.Sprintf("%s.%s", uuid.New().String(), strings.Split(handler.Header.Get("Content-Type"), "/")[1])

	newFile, err := os.Create(filepath.Join(a.Env.StoragePath, genFileName))
	if err != nil {
		fmt.Println(err)
	}
	defer func() {
		_ = newFile.Close()
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		ServerError(err, w)
		return
	}

	if _, err := newFile.Write(fileBytes); err != nil {
		ServerError(err, w)
		return
	}

	if _, err := a.addFile(newFile.Name()); err != nil {
		ServerError(err, w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(ResponseStatus{Status: true}); err != nil {
		ServerError(err, w)
		return
	}
}
func (a *App) list(w http.ResponseWriter, _ *http.Request) {
	files, err := a.getFiles()
	if err != nil {
		ServerError(err, w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(files); err != nil {
		ServerError(err, w)
		return
	}
}
func (a *App) preview(w http.ResponseWriter, r *http.Request) {

	key := r.URL.Query().Get("id")
	if key == "" {
		log.Println(errors.New("url param 'id' is missing"))
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	fileId, err := strconv.Atoi(key)
	if err != nil {
		ServerError(err, w)
		return
	}

	file, err := a.getFile(fileId)
	if err != nil {
		ServerError(err, w)
		return
	}

	imgFile, err := os.Open(file.Path)
	defer func() {
		_ = imgFile.Close()
	}()

	if err != nil {
		ServerError(err, w)
		return
	}

	img, imgType, err := image.Decode(imgFile)
	if err != nil {
		ServerError(err, w)
		return
	}

	preview := resize.Resize(160, 0, img, resize.Lanczos3)

	buffer := new(bytes.Buffer)
	if err := jpeg.Encode(buffer, preview, nil); err != nil {
		ServerError(err, w)
		return
	}

	w.Header().Set("Content-Type", fmt.Sprintf("image/%s", imgType))
	w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))

	if _, err := w.Write(buffer.Bytes()); err != nil {
		ServerError(err, w)
		return
	}
}
