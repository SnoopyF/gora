package main

import (
	"log"
	"net/http"
)

func ServerError(err error,w http.ResponseWriter)  {
	log.Println(err)
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}
