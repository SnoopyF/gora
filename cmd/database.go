package main

import (
	"fmt"
	"log"
)

type File struct {
	Id   int    `json:"id"`
	Path string `json:"path"`
}

func (a *App) delFile(fileId int) error {
	_, err := a.DB.Exec("delete from files where id=$1", fileId)
	if err != nil {
		return err
	}

	return nil
}

func (a *App) addFile(filePath string) (int64, error) {
	result, err := a.DB.Exec("insert into files (path) values ($1)", filePath)
	if err != nil {
		return 0, err
	}

	lastId, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return lastId, nil
}

func (a *App) getFile(id int) (*File, error) {
	var file File
	if err := a.DB.QueryRow("select * from files where id=$1", id).Scan(&file.Id, &file.Path); err != nil {
		log.Println(err)
		return nil, err
	}
	return &file, nil
}

func (a *App) getFiles() ([]File, error) {
	rows, err := a.DB.Query("select * from files")
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer func() {
		_ = rows.Close()
	}()

	var files []File

	for rows.Next() {
		p := File{}
		err := rows.Scan(&p.Id, &p.Path)
		if err != nil {
			fmt.Println(err)
			continue
		}
		files = append(files, p)
	}

	return files, nil
}
