# Upload
```
curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@examples/download.jpeg" http://localhost:8000/upload 
```
```
HTTP/1.1 100 Continue
HTTP/1.1 200 OK
Server: nginx/1.15.8
Date: Wed, 19 Feb 2020 13:24:28 GMT
Content-Type: application/json
Content-Length: 16
Connection: keep-alive

{"status":true}
```

# List
```
curl -i  http://localhost:8000/list
```
```
HTTP/1.1 200 OK
Server: nginx/1.15.8
Date: Wed, 19 Feb 2020 13:24:33 GMT
Content-Type: application/json
Content-Length: 71
Connection: keep-alive

[{"id":2,"path":"/storage/d20f24d9-e702-485e-9cbc-b797bbe4d81b.jpeg"}]
```

# Preview
```
curl -i  http://localhost:8000/preview?id=2
```
```
HTTP/1.1 200 OK
Server: nginx/1.15.8
Date: Wed, 19 Feb 2020 13:24:45 GMT
Content-Type: image/jpeg
Content-Length: 3379
Connection: keep-alive
Expires: Thu, 31 Dec 2037 23:55:55 GMT
Cache-Control: max-age=315360000

Warning: Binary output can mess up your terminal. Use "--output -" to tell 
Warning: curl to output it to your terminal anyway, or consider "--output 
Warning: <FILE>" to save to a file.
```
# Delete
```
curl -i  http://localhost:8000/delete?id=2
```
```
HTTP/1.1 200 OK
Server: nginx/1.15.8
Date: Wed, 19 Feb 2020 13:46:21 GMT
Content-Type: text/plain; charset=utf-8
Content-Length: 16
Connection: keep-alive

{"status":true}

```