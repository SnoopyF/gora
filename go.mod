module gora

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
)
